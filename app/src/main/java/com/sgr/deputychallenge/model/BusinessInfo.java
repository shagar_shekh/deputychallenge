package com.sgr.deputychallenge.model;

import java.io.Serializable;

/**
 * Created by Shagar Shekh on 8/21/2017.
 */

public class BusinessInfo implements Serializable {
    private String name;
    private String logo;

    public BusinessInfo(String name, String logo) {
        this.name = name;
        this.logo = logo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}
