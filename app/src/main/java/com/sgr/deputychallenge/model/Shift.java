package com.sgr.deputychallenge.model;

import java.io.Serializable;

/**
 * Created by Shagar Shekh on 8/19/2017.
 */

public class Shift implements Serializable {
    String time;
    String latitude;
    String longitude;

    public Shift(String time, String latitude, String longitude) {
        this.time = time;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
