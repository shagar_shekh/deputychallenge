package com.sgr.deputychallenge.model;

import java.io.Serializable;

/**
 * Created by Shagar Shekh on 8/20/2017.
 */

public class ShiftDetails implements Serializable {
    int id;
    String start;
    String end;
    String startLatitude;
    String startLongitude;
    String endLatitude;
    String endLongitude;
    String image;

    public ShiftDetails() {
    }

    public ShiftDetails(int id, String start, String end, String startLatitude, String startLongitude, String endLatitude, String endLongitude, String image) {
        this.id = id;
        this.start = start;
        this.end = end;
        this.startLatitude = startLatitude;
        this.startLongitude = startLongitude;
        this.endLatitude = endLatitude;
        this.endLongitude = endLongitude;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getStartLatitude() {
        return startLatitude;
    }

    public void setStartLatitude(String startLatitude) {
        this.startLatitude = startLatitude;
    }

    public String getStartLongitude() {
        return startLongitude;
    }

    public void setStartLongitude(String startLongitude) {
        this.startLongitude = startLongitude;
    }

    public String getEndLatitude() {
        return endLatitude;
    }

    public void setEndLatitude(String endLatitude) {
        this.endLatitude = endLatitude;
    }

    public String getEndLongitude() {
        return endLongitude;
    }

    public void setEndLongitude(String endLongitude) {
        this.endLongitude = endLongitude;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
