package com.sgr.deputychallenge.db;

import android.provider.BaseColumns;

public final class DBDeputy {

    public static final String SELECT_SHIFT_DETAILS = "SELECT * FROM " + ShiftDetailTable.TABLE_NAME + " ;";

    private DBDeputy() {
    }

    public static class ShiftDetailTable implements BaseColumns {
        public static final String TABLE_NAME = "shift_details";
        public static final String COLUMN_ID = "shift_id";
        public static final String COLUMN_START = "start";
        public static final String COLUMN_END = "end";
        public static final String COLUMN_START_LAT = "startLatitude";
        public static final String COLUMN_START_LON = "startLongitude";
        public static final String COLUMN_END_LAT = "endLatitude";
        public static final String COLUMN_END_LON = "endLongitude";
        public static final String COLUMN_IMG = "image";


        public static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " +
                TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_ID + " INTEGER, " +
                COLUMN_START + " TEXT, " +
                COLUMN_END + " TEXT, " +
                COLUMN_START_LAT + " TEXT, " +
                COLUMN_START_LON + " TEXT, " +
                COLUMN_END_LAT + " TEXT, " +
                COLUMN_END_LON + " TEXT, " +
                COLUMN_IMG + " TEXT " + ")";
    }
}
