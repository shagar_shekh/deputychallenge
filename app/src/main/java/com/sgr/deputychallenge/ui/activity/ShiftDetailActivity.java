package com.sgr.deputychallenge.ui.activity;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sgr.deputychallenge.R;
import com.sgr.deputychallenge.model.ShiftDetails;
import com.sgr.deputychallenge.util.AppConstants;
import com.sgr.deputychallenge.util.AppUtil;
import com.squareup.picasso.Picasso;

public class ShiftDetailActivity extends AppCompatActivity implements OnMapReadyCallback {
    private CollapsingToolbarLayout mCollapsingToolbar;
    private EditText mEtStartTime, mEtEndTime;
    private ImageView mBackdropImg;

    private GoogleMap mMap;
    private ShiftDetails mShiftDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shift_detail);

        mShiftDetails = (ShiftDetails) getIntent().getSerializableExtra(AppConstants.BUNDLE_KEY_SHIFT_DETAILS);

        mEtStartTime = (EditText) findViewById(R.id.eTStartTime);
        mEtEndTime = (EditText) findViewById(R.id.eTEndTime);
        mBackdropImg = (ImageView) findViewById(R.id.backdrop);
        mCollapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbar);

        mCollapsingToolbar.setTitle("Shift Details");

        if (mShiftDetails != null)
            setShiftDetails();

        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng startLatLong = new LatLng(AppUtil.parseDouble(mShiftDetails.getStartLatitude()), AppUtil.parseDouble(mShiftDetails.getStartLongitude()));
        LatLng endLatLong = new LatLng(AppUtil.parseDouble(mShiftDetails.getEndLatitude()), AppUtil.parseDouble(mShiftDetails.getEndLongitude()));
        mMap.addMarker(new MarkerOptions().position(startLatLong).title("Shift " + mShiftDetails.getId() + " : Start").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
        mMap.addMarker(new MarkerOptions().position(endLatLong).title("Shift " + mShiftDetails.getId() + " : End"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(startLatLong));
    }

    private void setShiftDetails() {
        mEtStartTime.setText(AppUtil.timeISO8061ToDateTimeStr(mShiftDetails.getStart()));
        mEtEndTime.setText(AppUtil.timeISO8061ToDateTimeStr(mShiftDetails.getEnd()));
        Picasso.with(getApplicationContext()).load(mShiftDetails.getImage()).into(mBackdropImg);
    }

}
