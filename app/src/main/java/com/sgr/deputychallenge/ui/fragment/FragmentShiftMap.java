package com.sgr.deputychallenge.ui.fragment;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sgr.deputychallenge.R;
import com.sgr.deputychallenge.api.ShiftApi;
import com.sgr.deputychallenge.model.ShiftDetails;
import com.sgr.deputychallenge.ui.activity.ShiftDetailActivity;
import com.sgr.deputychallenge.util.AppConstants;
import com.sgr.deputychallenge.util.AppUtil;

import java.util.ArrayList;

/**
 * Created by Shagar Shekh on 8/20/2017.
 */

public class FragmentShiftMap extends Fragment {
    private GoogleMap mMap;
    MapView mapView;
    Context mContext;
    ArrayList<ShiftDetails> shiftList;

    ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_shift_map, container, false);
        mContext = container.getContext();
        mapView = rootView.findViewById(R.id.mapView);
        setUpMap(savedInstanceState);

        return rootView;
    }

    private void setUpMap(Bundle savedInstanceState) {
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
            mapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    mMap = googleMap;
                    mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                        @Override
                        public void onInfoWindowClick(Marker marker) {
                            for (ShiftDetails shiftDetails : shiftList) {
                                if (String.valueOf(marker.getSnippet()).equalsIgnoreCase(String.valueOf(shiftDetails.getId()))) {
                                    Intent intent = new Intent(mContext, ShiftDetailActivity.class);
                                    intent.putExtra(AppConstants.BUNDLE_KEY_SHIFT_DETAILS, shiftDetails);
                                    startActivity(intent);
                                }
                            }
                        }
                    });
                    if (!AppUtil.isNetworkConnected(mContext)) {
                        Snackbar.make(getActivity().findViewById(android.R.id.content), "No Internet Connection", Snackbar.LENGTH_LONG).show();
                    } else {
                        new ShiftTask().execute();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void putShiftMarkerOnMap(ArrayList<ShiftDetails> shiftList) {
        for (ShiftDetails shiftDetails : shiftList) {
            LatLng latLng = new LatLng(Double.parseDouble(shiftDetails.getStartLatitude()), Double.parseDouble(shiftDetails.getStartLongitude()));
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.title("Shift ID" + shiftDetails.getId());
            markerOptions.snippet("" + shiftDetails.getId());
            mMap.addMarker(markerOptions);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
        }
    }

    private class ShiftTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage(mContext.getResources().getString(R.string.sending_request));
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            shiftList = ShiftApi.getShiftDetailsLog();
            return "Executed";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (shiftList != null && !shiftList.isEmpty()) {
                putShiftMarkerOnMap(shiftList);
            }
        }
    }
}
