package com.sgr.deputychallenge.ui.fragment;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sgr.deputychallenge.R;
import com.sgr.deputychallenge.api.ShiftApi;
import com.sgr.deputychallenge.model.ShiftDetails;
import com.sgr.deputychallenge.ui.activity.ShiftDetailActivity;
import com.sgr.deputychallenge.ui.adapter.AdapterShiftLog;
import com.sgr.deputychallenge.util.AppConstants;
import com.sgr.deputychallenge.util.AppUtil;
import com.sgr.deputychallenge.util.RecyclerItemClickListener;

import java.util.ArrayList;

/**
 * Created by Shagar Shekh on 8/20/2017.
 */

public class FragmentShiftLog extends Fragment {
    private Context mContext;
    private RecyclerView mRvShiftLog;
    private AdapterShiftLog mAdapterShiftLog;

    private ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_shift_log, container, false);
        mContext = container.getContext();
        mRvShiftLog = rootView.findViewById(R.id.rVShiftLog);

        if (!AppUtil.isNetworkConnected(mContext)) {
            Snackbar.make(getActivity().findViewById(android.R.id.content), "No Internet Connection! Showing from Local DB", Snackbar.LENGTH_LONG).show();
            refreshListWithData(ShiftApi.getShiftDetailsListFromBD(mContext));
        } else {
            new ShiftTask().execute();
        }

        return rootView;
    }

    private void refreshListWithData(final ArrayList<ShiftDetails> shiftList) {
        mAdapterShiftLog = new AdapterShiftLog(mContext, shiftList);
        mRvShiftLog.setAdapter(mAdapterShiftLog);
        mRvShiftLog.setLayoutManager(new LinearLayoutManager(mContext));
        mRvShiftLog.setItemAnimator(new DefaultItemAnimator());
        mRvShiftLog.addOnItemTouchListener(
                new RecyclerItemClickListener(mContext, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent intent = new Intent(mContext, ShiftDetailActivity.class);
                        intent.putExtra(AppConstants.BUNDLE_KEY_SHIFT_DETAILS, shiftList.get(position));
                        startActivity(intent);
                    }
                })
        );
        mAdapterShiftLog.notifyDataSetChanged();
    }

    private class ShiftTask extends AsyncTask<String, Void, String> {
        ArrayList<ShiftDetails> shiftList;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage(mContext.getResources().getString(R.string.sending_request));
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            shiftList = ShiftApi.getShiftDetailsLog();
            return "Executed";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (shiftList != null && !shiftList.isEmpty()) {
                refreshListWithData(shiftList);
            }
        }
    }
}
