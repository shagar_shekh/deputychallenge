package com.sgr.deputychallenge.ui.fragment;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.sgr.deputychallenge.R;
import com.sgr.deputychallenge.api.ShiftApi;
import com.sgr.deputychallenge.model.Shift;
import com.sgr.deputychallenge.model.ShiftDetails;
import com.sgr.deputychallenge.util.AppPreferences;
import com.sgr.deputychallenge.util.AppUtil;
import com.sgr.deputychallenge.util.GPSTracker;

import java.util.ArrayList;

/**
 * Created by Shagar Shekh on 8/20/2017.
 */

public class FragmentShiftSchedule extends Fragment {
    Context mContext;
    Button btnStartShift, btnEndShift;
    EditText eTShiftStatus, eTStartTime, eTEndTime;

    private GPSTracker gpsTracker;
    private Location mLocation;
    double latitude, longitude;

    ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_shift_schedule, container, false);
        mContext = container.getContext();
        btnStartShift = rootView.findViewById(R.id.btnStartShift);
        btnEndShift = rootView.findViewById(R.id.btnEndShift);
        eTShiftStatus = rootView.findViewById(R.id.eTShiftStatus);
        eTStartTime = rootView.findViewById(R.id.eTStartTime);
        eTEndTime = rootView.findViewById(R.id.eTEndTime);

        btnStartShift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Shift shift = prepareShift();
                if (shift != null) {
                    new ShiftTask(shift, true).execute();
                }
            }
        });

        btnEndShift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Shift shift = prepareShift();
                if (shift != null) {
                    new ShiftTask(shift, false).execute();
                }
            }
        });

        updateShiftDetailsUI();

        return rootView;
    }

    public Shift prepareShift() {
        gpsTracker = new GPSTracker(getActivity().getApplicationContext());
        mLocation = gpsTracker.getLocation();
        Shift shift = null;

        if (AppUtil.hasPermission(mContext)) {
            if (mLocation != null) {
                latitude = mLocation.getLatitude();
                longitude = mLocation.getLongitude();
                String time = AppUtil.timeMillisToISO8061(System.currentTimeMillis());

                shift = new Shift(time, String.valueOf(latitude), String.valueOf(longitude));
            } else {
                Snackbar.make(getActivity().findViewById(android.R.id.content), mContext.getResources().getString(R.string.location_enable_msg), Snackbar.LENGTH_LONG).show();
            }
        } else {
            AppUtil.runtimePermission(getActivity(), mContext);
        }

        if (!AppUtil.isNetworkConnected(mContext)) {
            Snackbar.make(getActivity().findViewById(android.R.id.content), mContext.getResources().getString(R.string.internet_invalid), Snackbar.LENGTH_LONG).show();
            shift = null;
        }

        return shift;
    }

    private void updateShiftDetailsUI() {
        eTShiftStatus.setText(String.valueOf(AppPreferences.getString(mContext, AppPreferences.Key.SHIFT_STATUS)));

        Shift startShift = ShiftApi.getCurrentShiftLocal(mContext, AppPreferences.Key.LATEST_SHIFT_START);
        Shift endShift = ShiftApi.getCurrentShiftLocal(mContext, AppPreferences.Key.LATEST_SHIFT_END);

        String startTime = startShift != null ? startShift.getTime() : "";
        String endTime = endShift != null ? endShift.getTime() : "";

        eTStartTime.setText(AppUtil.timeISO8061ToDateTimeStr(startTime));
        eTEndTime.setText(AppUtil.timeISO8061ToDateTimeStr(endTime));
    }

    private class ShiftTask extends AsyncTask<String, Void, String> {
        Shift shift;
        boolean isStartShift;
        String response;

        public ShiftTask(Shift shift, boolean isStartShift) {
            this.shift = shift;
            this.isStartShift = isStartShift;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage(mContext.getResources().getString(R.string.sending_request));
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            response = isStartShift ? ShiftApi.startShift(shift) : ShiftApi.endShift(shift);
            return "Executed";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            String shiftStatus = "";
            if (response.contains("Start shift - All good") || response.contains("Nope, shift already in progress")) {
                shiftStatus = "Shift In Progress";
            } else {
                shiftStatus = "No Shift In Progress";
            }

            if (response.contains("All good")) {
                if (isStartShift) {
                    ShiftApi.saveCurrentShiftLocal(mContext, shift, AppPreferences.Key.LATEST_SHIFT_START);
                    AppPreferences.saveAppPreferences(mContext, AppPreferences.Key.LATEST_SHIFT_END, "");
                } else {
                    ShiftApi.saveCurrentShiftLocal(mContext, shift, AppPreferences.Key.LATEST_SHIFT_END);
                    new UpdateDBTask().execute();
                }
            }

            AppPreferences.saveAppPreferences(mContext, AppPreferences.Key.SHIFT_STATUS, shiftStatus);
            updateShiftDetailsUI();
        }
    }

    private class UpdateDBTask extends AsyncTask<String, Void, String> {
        ArrayList<ShiftDetails> shiftList;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage(mContext.getResources().getString(R.string.uploading_local_db));
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            shiftList = ShiftApi.getShiftDetailsLog();
            return "Executed";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            if (shiftList != null && !shiftList.isEmpty()) {
                ShiftApi.saveShiftDetailsToDB(mContext, shiftList.get(shiftList.size() - 1));
            }
        }
    }
}
