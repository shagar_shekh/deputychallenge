package com.sgr.deputychallenge.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.sgr.deputychallenge.R;
import com.sgr.deputychallenge.model.ShiftDetails;
import com.sgr.deputychallenge.util.AppUtil;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

/**
 * Created by Shagar Shekh on 8/21/2017.
 */
public class AdapterShiftLog extends RecyclerView.Adapter<AdapterShiftLog.MyViewHolder>{
    private List<ShiftDetails> mData = Collections.emptyList();
    private LayoutInflater mInflater;
    private Context mContext;

    public AdapterShiftLog(Context context, List<ShiftDetails> data) {
        this.mContext = context;
        mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.adapter_shift_log, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final ShiftDetails current = mData.get(position);
        Picasso.with(mContext).load(current.getImage()).networkPolicy(NetworkPolicy.NO_CACHE)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .into(holder.mIvImage);

        holder.mTvId.setText("ID : "+current.getId());
        holder.mEtStartTime.setText(AppUtil.timeISO8061ToDateTimeStr(current.getStart()));
        holder.mEtEndTime.setText(AppUtil.timeISO8061ToDateTimeStr(current.getEnd()));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView mIvImage;
        TextView mTvId;
        EditText mEtStartTime;
        EditText mEtEndTime;

        public MyViewHolder(View itemView) {
            super(itemView);
            mTvId = itemView.findViewById(R.id.tVID);
            mIvImage = itemView.findViewById(R.id.iVImage);
            mEtStartTime = itemView.findViewById(R.id.eTStartTime);
            mEtEndTime = itemView.findViewById(R.id.eTEndTime);
        }
    }
}
