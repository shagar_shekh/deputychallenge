package com.sgr.deputychallenge.util;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntityHC4;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.logging.Logger;

public class ServerCall {
    private static ServerCall instance = new ServerCall();

    private ServerCall() {
    }

    public static ServerCall getInstance() {
        return instance;
    }

    public String CallByPOST(String serverUrl, String gsonStr) {
        StringBuffer result = new StringBuffer();
        try {
            if (!AppUtil.isEmptyStr(serverUrl) && gsonStr != null) {
                HttpClient client = HttpClientBuilder.create().build();
                HttpPost httpPost = new HttpPost(serverUrl);
                httpPost.setHeader(AppConstants.AUTH_HEADER_KEY, AppConstants.AUTH_HEADER_VALUE);
                httpPost.setHeader("Content-type", "application/json");
                StringEntityHC4 stringEntityHC4 = new StringEntityHC4(gsonStr.toString());
                httpPost.setEntity(stringEntityHC4);

                HttpResponse response = client.execute(httpPost);
                System.out.println("Response Code : " + response.getStatusLine().getStatusCode());
                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

                String line = "";
                while ((line = rd.readLine()) != null) {
                    result.append(line);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return String.valueOf(result.toString());
    }

    public String CallByGET(String url) {
        StringBuffer result = new StringBuffer();
        try {
            if (!AppUtil.isEmptyStr(url)) {
                HttpClient client = HttpClientBuilder.create().build();
                HttpGet get = new HttpGet(url);
                get.setHeader(AppConstants.AUTH_HEADER_KEY, AppConstants.AUTH_HEADER_VALUE);
                HttpResponse response = client.execute(get);
                System.out.println("Response Code : " + response.getStatusLine().getStatusCode());

                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

                String line = "";
                while ((line = rd.readLine()) != null) {
                    result.append(line);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return String.valueOf(result);
    }
}
