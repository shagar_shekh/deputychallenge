package com.sgr.deputychallenge.util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Shekh Shagar
 *         Date: 11 04 - 2016 | Time: 2:20 AM
 *         For any improvement and suggestion, please ping me at shagar.shekh@gmail.com
 * @skype: shekh.shagar
 */
public class AppUtil {
    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public static boolean isEmptyStr(String string) {
        try {
            return (string == null || string.trim().isEmpty()) ? true : false;
        } catch (Exception e) {
            return true;
        }
    }

    public static double parseDouble(String stringValue) {
        try {
            return Double.parseDouble(stringValue);
        } catch (Exception e) {
            return 0.0;
        }
    }

    public static String timeMillisToISO8061(long timeMillisecond) {
        try {
            Date dateTime = new Date(timeMillisecond);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
            return String.valueOf(format.format(dateTime));
        } catch (Exception e) {
            return "";
        }
    }

    @NonNull
    public static Long timeISO8061ToMillisecond(String inputString) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
            Date date = format.parse(inputString.trim());
            return date.getTime();
        } catch (Exception e) {
            return 0l;
        }
    }

    @NonNull
    public static String timeISO8061ToDateTimeStr(String inputString) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
            Date date = format.parse(inputString.trim());

            SimpleDateFormat sdf2 = new SimpleDateFormat("dd MMM yyyy hh:mm a");
            return String.valueOf(sdf2.format(date));
        } catch (Exception e) {
            return "";
        }
    }

    public static String[] PERMISSIONS = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};

    public static void runtimePermission(Activity activity, Context context) {
        if (!hasPermission(context)) {
            ActivityCompat.requestPermissions(activity, PERMISSIONS, 1);
        }
    }

    public static boolean hasPermission(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && PERMISSIONS != null) {
            for (String permission : PERMISSIONS) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
}
