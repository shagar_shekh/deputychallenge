package com.sgr.deputychallenge.util;
/**
 * @author Shekh Shagar
 * Date: 11 04 - 2016 | Time: 03:09 AM
 * For any improvement and suggestion, please ping me at shagar.shekh@gmail.com
 * @skype: shekh.shagar
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class AppPreferences {
    private static AppPreferences mInstance;

    private static AppPreferences getInstance() {
        if (mInstance == null) {
            mInstance = new AppPreferences();
        }
        return mInstance;
    }

    private static SharedPreferences getPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    private AppPreferences() {
    }

    public static boolean saveAppPreferences(Context context, String key, String value) {
        return getInstance().getPreferences(context).edit().putString(key, value).commit();
    }

    public static boolean saveAppPreferences(Context context, String key, int value) {
        return getInstance().getPreferences(context).edit().putInt(key, value).commit();
    }

    public static boolean saveAppPreferences(Context context, String key, long value) {
        return getInstance().getPreferences(context).edit().putLong(key, value).commit();
    }

    public static boolean saveAppPreferences(Context context, String key, boolean value) {
        return getInstance().getPreferences(context).edit().putBoolean(key, value).commit();
    }

    public static String getString(Context context, String key) {
        return getInstance().getPreferences(context).getString(key, "");
    }

    public static int getInt(Context context, String key) {
        return getInstance().getPreferences(context).getInt(key, 1);
    }

    public static long getLong(Context context, String key) {
        return getInstance().getPreferences(context).getLong(key, 1);
    }

    public static boolean getBoolean(Context context, String key) {
        return getInstance().getPreferences(context).getBoolean(key, false);
    }

    public static class Key {
        public static final String SHIFT_STATUS = "SHIFT_STATUS";
        public static final String LATEST_SHIFT_START = "LATEST_SHIFT_START";
        public static final String LATEST_SHIFT_END = "LATEST_SHIFT_END";
        public static final String BUSINESS_INFO = "BUSINESS_INFO";
    }
}