package com.sgr.deputychallenge.util;

/**
 * Created by Shagar Shekh on 8/19/2017.
 */

public class AppConstants {
    public static final String BASE_URL = "https://apjoqdqpi3.execute-api.us-west-2.amazonaws.com/dmc/";
    public static final String AUTH_HEADER_KEY = "Authorization";
    public static final String AUTH_HEADER_VALUE = "Deputy 6fdfb0aeb1d347c4c047124332c6f0cd1ed2e196";

    public static final String URL_START_SHIFT = BASE_URL + "shift/start";
    public static final String URL_END_SHIFT = BASE_URL + "shift/end";
    public static final String URL_SHIFTS = BASE_URL + "shifts";
    public static final String URL_BUSINESS = BASE_URL + "business";

    public static final String BUNDLE_KEY_SHIFT_DETAILS = "SHIFT_DETAILS";

}
