package com.sgr.deputychallenge.api;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sgr.deputychallenge.db.DBDeputy;
import com.sgr.deputychallenge.db.DBSQLiteHelper;
import com.sgr.deputychallenge.model.Shift;
import com.sgr.deputychallenge.model.ShiftDetails;
import com.sgr.deputychallenge.util.AppConstants;
import com.sgr.deputychallenge.util.AppPreferences;
import com.sgr.deputychallenge.util.AppUtil;
import com.sgr.deputychallenge.util.ServerCall;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shagar Shekh on 8/20/2017.
 */

public class ShiftApi {

    /**
     * Starts a shift
     * @param shift Starting Shift object
     * @return
     */
    public static String startShift(Shift shift) {
        String response = ServerCall.getInstance().CallByPOST(AppConstants.URL_START_SHIFT, new Gson().toJson(shift));
        return response;
    }

    /**
     * Ends a shift
     * @param shift Ending Shift object
     * @return
     */
    public static String endShift(Shift shift) {
        String response = ServerCall.getInstance().CallByPOST(AppConstants.URL_END_SHIFT, new Gson().toJson(shift));
        return response;
    }

    /**
     * Provides business info
     * @return
     */
    public static String getBusinessInfo() {
        String response = ServerCall.getInstance().CallByGET(AppConstants.URL_BUSINESS);
        return response;
    }

    /**
     * Provides list of ShiftDetails
     * @return
     */
    public static ArrayList<ShiftDetails> getShiftDetailsLog() {
        ArrayList<ShiftDetails> shiftList = null;
        String response = ServerCall.getInstance().CallByGET(AppConstants.URL_SHIFTS);
        if (!(AppUtil.isEmptyStr(response)))
            shiftList = new Gson().fromJson(response, new TypeToken<List<ShiftDetails>>() {
            }.getType());

        return shiftList;
    }

    /**
     * Saves current shift to SharedPreference
     * @param context App Context
     * @param shift so store
     * @param keyToStore SharedPreference key
     */
    public static void saveCurrentShiftLocal(Context context, Shift shift, String keyToStore) {
        String json = new Gson().toJson(shift);
        AppPreferences.saveAppPreferences(context, keyToStore, json);
    }

    /**
     * get shift details from local database
     * @param context App Context
     * @param keyToRetrieve SharedPreference key
     * @return
     */
    public static Shift getCurrentShiftLocal(Context context, String keyToRetrieve) {
        String json = AppPreferences.getString(context, keyToRetrieve);
        Shift shift = null;
        if (!AppUtil.isEmptyStr(json)) {
            Type type = (Type) new TypeToken<Shift>() {
            }.getType();
            shift = new Gson().fromJson(json, type);
        }
        return shift;
    }

    /**
     * Save ShiftDetails object to local database
     * @param context App Context
     * @param shiftDetails to store
     */
    public static void saveShiftDetailsToDB(Context context, ShiftDetails shiftDetails) {
        SQLiteDatabase database = new DBSQLiteHelper(context).getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(DBDeputy.ShiftDetailTable.COLUMN_ID, shiftDetails.getId());
        values.put(DBDeputy.ShiftDetailTable.COLUMN_START, shiftDetails.getStart());
        values.put(DBDeputy.ShiftDetailTable.COLUMN_END, shiftDetails.getEnd());
        values.put(DBDeputy.ShiftDetailTable.COLUMN_START_LAT, shiftDetails.getStartLatitude());
        values.put(DBDeputy.ShiftDetailTable.COLUMN_START_LON, shiftDetails.getStartLongitude());
        values.put(DBDeputy.ShiftDetailTable.COLUMN_END_LAT, shiftDetails.getEndLatitude());
        values.put(DBDeputy.ShiftDetailTable.COLUMN_END_LON, shiftDetails.getEndLongitude());
        values.put(DBDeputy.ShiftDetailTable.COLUMN_IMG, shiftDetails.getImage());

        database.insert(DBDeputy.ShiftDetailTable.TABLE_NAME, null, values);
    }

    /**
     * Save ShiftDetails list to local database
     * @param context App Context
     * @param shiftDetailsList to store
     */
    public static void saveShiftDetailsToDB(Context context, ArrayList<ShiftDetails> shiftDetailsList) {
        SQLiteDatabase database = new DBSQLiteHelper(context).getWritableDatabase();
        ContentValues values = new ContentValues();

        for (ShiftDetails shiftDetails : shiftDetailsList) {

            values.put(DBDeputy.ShiftDetailTable.COLUMN_ID, shiftDetails.getId());
            values.put(DBDeputy.ShiftDetailTable.COLUMN_START, shiftDetails.getStart());
            values.put(DBDeputy.ShiftDetailTable.COLUMN_END, shiftDetails.getEnd());
            values.put(DBDeputy.ShiftDetailTable.COLUMN_START_LAT, shiftDetails.getStartLatitude());
            values.put(DBDeputy.ShiftDetailTable.COLUMN_START_LON, shiftDetails.getStartLongitude());
            values.put(DBDeputy.ShiftDetailTable.COLUMN_END_LAT, shiftDetails.getEndLatitude());
            values.put(DBDeputy.ShiftDetailTable.COLUMN_END_LON, shiftDetails.getEndLongitude());
            values.put(DBDeputy.ShiftDetailTable.COLUMN_IMG, shiftDetails.getImage());

            database.insert(DBDeputy.ShiftDetailTable.TABLE_NAME, null, values);
        }
    }

    /**
     * Retrieve ShiftDetails log from local database
     * @param context App Context
     * @return
     */
    public static ArrayList<ShiftDetails> getShiftDetailsListFromBD(Context context) {
        ArrayList<ShiftDetails> shiftDetailsList = new ArrayList<ShiftDetails>();

        SQLiteDatabase database = new DBSQLiteHelper(context).getReadableDatabase();
        Cursor cursor = database.rawQuery(DBDeputy.SELECT_SHIFT_DETAILS, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            while (cursor.moveToNext()) {
                ShiftDetails shiftDetails = new ShiftDetails();

                shiftDetails.setId(cursor.getInt(cursor.getColumnIndexOrThrow(DBDeputy.ShiftDetailTable.COLUMN_ID)));
                shiftDetails.setStart(cursor.getString(cursor.getColumnIndexOrThrow(DBDeputy.ShiftDetailTable.COLUMN_START)));
                shiftDetails.setEnd(cursor.getString(cursor.getColumnIndexOrThrow(DBDeputy.ShiftDetailTable.COLUMN_END)));
                shiftDetails.setStartLatitude(cursor.getString(cursor.getColumnIndexOrThrow(DBDeputy.ShiftDetailTable.COLUMN_START_LAT)));
                shiftDetails.setStartLongitude(cursor.getString(cursor.getColumnIndexOrThrow(DBDeputy.ShiftDetailTable.COLUMN_START_LON)));
                shiftDetails.setEndLatitude(cursor.getString(cursor.getColumnIndexOrThrow(DBDeputy.ShiftDetailTable.COLUMN_END_LAT)));
                shiftDetails.setEndLongitude(cursor.getString(cursor.getColumnIndexOrThrow(DBDeputy.ShiftDetailTable.COLUMN_END_LON)));
                shiftDetails.setImage(cursor.getString(cursor.getColumnIndexOrThrow(DBDeputy.ShiftDetailTable.COLUMN_IMG)));

                shiftDetailsList.add(shiftDetails);
            }
        }
        return shiftDetailsList;
    }

}
